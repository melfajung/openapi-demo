<?php
namespace app\modules\v1;

use Yii;
use yii\base\BootstrapInterface;
class Module extends \yii\base\Module implements BootstrapInterface {
    public $controllerNamespace = 'app\modules\v1\controllers';

    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application)
        {
            Yii::$app->urlManager->addRules([
                'v1/<controller>/<action>' => 'v1/<controller>/<action>',
                'v1/<controller>/<reqPath:.+>' => 'v1/<controller>/no-action',
            ]);
        }
    }
}

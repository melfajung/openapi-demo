<?php
namespace app\modules\v1\controllers;

use MongoDB\BSON\UTCDateTime;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotfoundHttpException;
use yii\web\Response;

use app\models\User;

class UsersController extends BaseController {  
  /**
   * List all users
   */
  public function actionIndex()
  {
    $this->requireMethod('get');

    $lst = User::find()->all();

    $result = [];
    foreach($lst as $model)
    {
      unset($model->password);
      $result[] = $model;
    }
    
    return $result;
  }

  /**
   * Register new user using username and password
   */
  public function actionRegister()
  {
    $this->requireMethod('post');

    $request = Yii::$app->request;
    $errorMessage = null;

    $user = new User();
    $user->username = $request->post('username');
    $user->password = Yii::$app->security->generatePasswordHash($request->post('password'));
    $user->name = $request->post('name');
    $user->email = $request->post('email');
    $user->status = User::STATUS_NEW;
    $user->roles = [];
    $user->scopes = ['user'];
    $user->createTime = new UTCDateTime();
    $user->activationKey = rand(1000, 9999);
  
    if(empty($errorMessage) && !$user->validate())
    {
      var_dump($user->getErrors());exit;
      $errorMessage = "Data validation failed: " . join(', ', array_keys($user->getErrors()));
    }

    if (empty($errorMessage) && !empty($user->username))
    {
      $cnt = User::find()
        ->where(['username' => $user->username])
        ->count();

      if ($cnt > 0)
        $errorMessage = 'Duplicated registration';
    }

    if (empty($errorMessage) && !empty($user->username))
    {
      $cnt = User::find()
        ->where(['username' => $user->username])
        ->count();

      if ($cnt > 0)
        $errorMessage = 'Duplicated registration';
    }

    if (empty($errorMessage) && !empty($user->email))
    {
      $cnt = User::find()
        ->where(['email' => $user->email])
        ->count();

      if ($cnt > 0)
        $errorMessage = 'Duplicated registration';
    }

    if (empty($errorMessage) && !empty($user->phone))
    {
      $cnt = User::find()
        ->where(['phone' => $user->phone])
        ->count();

      if ($cnt > 0)
        $errorMessage = 'Duplicated registration';
    }

    if (!empty($errorMessage))
      throw new BadRequestHttpException($errorMessage);

    $user->save();

    unset($user->password);
    return $user;
  }

  private function activate($id)
  {
    $this->requireMethod('get');

    $request = Yii::$app->request;
    $errorMessage = null;

    if (empty($errorMessage)) {
      $user = User::find()->where(['_id' => $id])->one();

      if (empty($user) || $user->status != User::STATUS_NEW)
      {
        $errorMessage = 'Invalid User';
      }
    }

    if (empty($errorMessage) && $user->activationKey != $request->get('activationKey'))
    {
      $errorMessage = 'Invalid activation key';
    }

    if (!empty($errorMessage))
      throw new BadRequestHttpException($errorMessage);

    $user->status = User::STATUS_ACTIVE;
    $user->save();

    $result = Yii::$app->mongodb->getCollection('user')->update([
      '_id' => ($user->_id),
    ], [
      '$unset' => [
        'activationKey' => 1,
      ]
    ]);

    unset($user->password, $user->activationKey);
    return $user;
  }

  protected function catchAll($actions)
  {
    if (count($actions) == 1)
    {
      return $this->getItem($actions[0]);
    }
    else
    {
      $userId = array_shift($actions);
      $user = User::find()->where(['_id' => $userId])->one();
      if (empty($user))
        throw new NotfoundHttpException('User not found');

      $action = array_shift($actions);
      switch($action) {
        case 'activate':
          return $this->activate($userId);
        break;
      }      
    }

    throw new BadRequestHttpException('Invalid request');
  }

  public function getItem($id)
  {
    $user = User::find()->where(['_id' => $id])->one();
    unset($user->password);

    return $user;
  }

  protected function validateRequest()
  {
    $unauthPaths = ["/users\/authenticate/", "/users\/register/", "/users\/[a-fA-F0-9]+\/activate/"];
    foreach($unauthPaths as $path)
    {
      if (preg_match($path, Yii::$app->request->pathinfo))
        return true;
    }

    return parent::validateRequest();
  }
}

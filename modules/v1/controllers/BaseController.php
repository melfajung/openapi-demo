<?php
namespace app\modules\v1\controllers;

use Exception;

use MongoDB\BSON\UTCDateTime;

use Yii;
use yii\base\InvalidParamException;
use yii\base\InvalidRouteException;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

use app\libs\Auth;
use app\libs\Json;

use app\models\User;

class BaseController extends Controller {
    protected $m; // request method
    protected $uid; // user id (from authorization token)
    protected $roles;   // user roles (from authorization token)
    protected $scopes;  // authenticated scopes (from authorization token)
    protected $user;  // persistent layer user instance

    /**
     * Get authenticated user model instance
     */
    public function getUser() {
        if (!isset($this->user))
        {
            $this->user = User::findById($this->uid);
        }

        return $this->user;
    }

    public function init() {
        parent::init();

        $request = Yii::$app->request;
        $request->enableCsrfValidation = false;
        $this->m = strtolower($request->method);

        if ($this->m == 'options') {
            $origin = $request->getOrigin();
            header('Access-Control-Allow-Origin: ' . (empty($origin) ? '*' : $origin));
            header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
            header('Access-Control-Allow-Headers: Authorization, Content-Type');
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 600');
            exit;
        }
    }

    /**
     * Get instance's proterties
     */
    protected function item($model)
    {
        return $model;
    }

    /**
     * Check if current is authorize using specified scope
     * @param string $scope scope to check
     * @return boolean
     */
    protected function inScope($scope)
    {
        return in_array($scope, $this->scopes);
    }

    /**
     * Check request method to be in specified list. Throw an exception if not valid
     * @param string|array $methods allowed method
     * @throws BadRequestHttpException
     */
    protected function requireMethod($methods)
    {
        if (!is_array($methods)) $methods = [$methods];

        if (!in_array($this->m, $methods))
        {
            array_walk($methods, function(&$m) {
                $m = strtoupper($m);
            });
            throw new BadRequestHttpException('Only ' . join(', ', $methods) . ' allowed');
        }

        return $this;
    }

    /**
     * check if current user has required permission or not. Throw an Exception if not
     * @param string $permission specified permission
     * @throws ForbiddenHttpException
     */
    protected function requirePermission($permission)
    {
        //throw new ForbiddenHttpException('Access Denied');

        return $this;
    }

    /**
     * check if current user are in required roles or not. Throw an exception if not
     * @param string[] $roles allowed roles
     * @throws ForBiddenHttpException
     */
    protected function requireRoles($roles)
    {
        $valid = false;
        foreach($roles as $role)
        {
            if (in_array($role, $this->roles))
            {
                $valid = true;
                break;
            }
        }
        if (!$valid)
            throw new ForbiddenHttpException('Need to be in roles: [' . join(', ', $roles) . ']');

        return $this;
    }

    /**
     * check if current user are in required scopes or not. Throw an exception if not
     * @param string[] $scopes allowed scopes
     * @throws ForBiddenHttpException
     */
    protected function requireScopes($scopes)
    {
        $valid = false;
        foreach($scopes as $scope)
        {
            if (in_array($scope, $this->scopes))
            {
                $valid = true;
                break;
            }
        }
        if (!$valid)
            throw new ForbiddenHttpException('Need to be in scope: [' . join(', ', $scopes) . ']');

        return $this;
    }

    public function runAction($id, $params = [])
    {
        $result = null;
        
        try {
            $response = Yii::$app->response;
            $response->charset = 'utf-8';
            $response->format = Response::FORMAT_JSON;
            $origin = Yii::$app->request->getOrigin();
            $response->headers->add('Access-Control-Allow-Origin', (empty($origin) ? '*' : $origin));
            $response->headers->add('Access-Control-Allow-Credentials', 'true');

            $this->validateRequest();
    
            $result = parent::runAction($id, $params);
        }
        catch(InvalidRouteException $ex)
        {
            if(method_exists($this, 'catchAll'))
            {
                $request = Yii::$app->request;
                $pathInfo = preg_replace('/^\/?v1\/' . $this->id . '\//', '', $request->pathInfo);
                $actions = preg_split('/\//', $pathInfo);
                try {
                    $result = $this->catchAll($actions);
                }
                catch(InvalidParamException|Exception $ex)
                {
                    $statusCode = $ex instanceof HttpException ? $ex->statusCode : 500;
                    Yii::$app->response->setStatusCode($statusCode);
                    $result = [
                        'success' => false,
                        'message' => $ex->getMessage()
                    ];
                }
                finally
                {
                    return Json::prepare($result);
                }   
            }
            else
                throw $ex;
        }
        catch (Exception $ex)
        {
            $statusCode = $ex instanceof HttpException ? $ex->statusCode : 500;
            Yii::$app->response->setStatusCode($statusCode);
            $result = [
                'success' => false,
                'message' => $ex->getMessage()
            ];
        }

        return Json::prepare($result);
    }

    protected function validateRequest()
    {
        if (in_array($this->m, ['options']))
            return true;

        $request = Yii::$app->request;
        $authHeader = $request->headers->get('Authorization');
        if (empty($authHeader))
        {
            throw new HttpException(401, 'Authorization required');
        }
        else
        {
            $auths = preg_split("/\s/", $authHeader);
            if ($auths[0] == 'Bearer' && isset($auths[1])) {
                $token = Auth::getJwtToken($auths[1]);

                if (empty($token))
                {
                    throw new HttpException(403, 'Access denied');
                }

                $this->uid = $token->getClaim('uid');
                $this->scopes = $token->getClaim('scopes');
                $this->roles = $token->getClaim('roles');

                return true;
            }
        }

        return false;
    }
}
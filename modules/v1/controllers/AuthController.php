<?php
namespace app\modules\v1\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Response;

use app\libs\Auth;

use app\models\User;
class AuthController extends BaseController
{
  public function actionLogin()
  {
    $this->requireMethod('post');
    
    $request = Yii::$app->request;
    $success = false;

    $username = $request->post('username');
    $password = $request->post('password');
    $duration = intval(min($request->post('duration', 3600), 864000));

    $user = User::findByUsername($username);
    if (empty($user))
      throw new BadRequestHttpException('Invalid user');

    if ($user->status != User::STATUS_ACTIVE)
      throw new BadRequestHttpException('User is not active');

    $scopes = $request->post('scopes');
    $scopes = array_filter($scopes, function($scope) {
      return in_array($scope, Auth::$validScopes);
    });
    if (empty($scopes))
      throw new BadRequestHttpException('Scope is invalid');

    foreach($scopes as $scope)
    {
      if (!in_array($scope, $user->scopes))
        throw new BadRequestHttpException('Scope is invalid');
    }

    if ($user->validatePassword($password))
    {
      $token = Auth::newToken($user, $scopes, $duration);
      $success = true;
    }

    if ($success)
    {
      return [
        'success' => $success,
        'roles' => $user->roles,
        'scopes' => $scopes,
        'token' => (string)$token,
        'userId' => $user->_id,
      ];
    }
    else
    {
      return [
        'success' => $success,
        'errorMessage' => 'Invalid Login',        
      ];
    }
  }

  protected function validateRequest()
  {
    return true;
  }

}

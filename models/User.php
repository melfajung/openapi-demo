<?php

namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for table "User".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property int $status
 * @property string|null $role
 * @property string $createTime
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $activeSession
 * 
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{
  /**
   * @return array list of attribute names.
   */
  public function attributes()
  {
    return ['_id', 'activationKey', 'activeSession', 'authChannel', 'authRefId', 'createTime', 'email', 'name', 'password', 'roles', 'scopes', 'status', 'username'];
  }

  /**
  * {@inheritdoc}
  */
  public function rules()
  {
    return [
      [['name', 'status', 'createTime'], 'required'],
      [['status'], 'integer'],
      [['createTime'], 'safe'],
      [['name'], 'string', 'max' => 50],
      [['username'], 'string', 'max' => 30],
      [['activeSession'], 'string', 'max' => 36],
    ];
  }

  // UserIdentity

  /**
   * {@inheritdoc}
   */
  public static function findIdentity($id)
  {
    $model = $model = self::find()->where(['_id' => $id])->one();
    
    return empty($model)?null:$model;
  }

  /**
   * {@inheritdoc}
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    $token = Auth::getJwtToken($token);
    
    if ($token instanceof \Lcobucci\JWT\Token)
    {
        return self::findIdentity($token->getClaim('uid'));
    }
  }

  /**
   * Finds user by username
   *
   * @param string $username
   * @return static|null
   */
  public static function findByUsername($username)
  {
    $model = self::find()->where(['username'=>$username])->one();

    return empty($model)?null:$model;
  }

  /**
   * {@inheritdoc}
   */
  public function getId()
  {
    return (string)$this->_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthKey()
  {
    return $this->authKey;
  }

  /**
   * {@inheritdoc}
   */
  public function validateAuthKey($authKey)
  {
    return $this->authKey === $authKey;
  }

  /**
   * Validates password
   *
   * @param string $password password to validate
   * @return bool if password provided is valid for current user
   */
  public function validatePassword($password)
  {
    return Yii::$app->getSecurity()->validatePassword($password, $this->password);
  }
    
  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password)
  {
      $this->password = Yii::$app->security->generatePasswordHash($password);
  }

  // business logic

  const STATUS_NEW = 0;
  const STATUS_ACTIVE = 10;
  const STATUS_INACTIVE = -1;

  public static $arrStatus = array(
      self::STATUS_NEW => 'ผู้ใช้ใหม่',
      self::STATUS_INACTIVE => 'ไม่ใช้งาน',
      self::STATUS_ACTIVE => 'ใช้งาน',
  );

  public static function findById($id)
  {
    return self::findIdentity($id);
  }

  protected function processData($attrs)
  {
    $processedAttrs = [];
    $unsetAttrs = [];

    foreach($attrs as $key => $value)
    {
      if(is_null($value))
      {
        $unsetAttrs[$key] = '';
        continue;
      }
        
      if (in_array($key, $this->safeAttributes()))
        $processedAttrs[$key] = $value;
    }
        
    $updates = [
      '$set' => $processedAttrs,
    ];
    if (!empty($unsetAttrs))
      $updates['$unset'] = $unsetAttrs;

    return $updates;
  }

  /**
   * Update model's attributes by specified attributes
   * @param string[] $attrs
   * @return User
   */
  public function updateData($attrs, $params = [])
  {
    $updates = $this->processData($attrs);

    if (!empty($attrs['roles']) && is_array($attrs['roles']))
    {
        $roles = array_values(array_unique(array_merge($attrs['roles'], $this->roles)));
        $updates['$set']['roles'] = $roles;
    }

    if (!empty($attrs['scopes']) && is_array($attrs['scopes']))
    {
        $scopes = array_filter($attrs['scopes'], function($scope) {
            return in_array($scope, Auth::$validScopes);
        });
        $scopes = array_values(array_unique(array_merge($scopes, $this->scopes)));
        $updates['$set']['scopes'] = $scopes;
    }

    if (isset($updates['$set']))
      $this->attributes = $updates['$set'];
    
    if (isset($attrs['$set']['roles']))
      $this->scopes = $attrs['$set']['roles'];
    if (isset($attrs['$set']['scopes']))
      $this->scopes = $attrs['$set']['scopes'];
  
  	$this->collection->update(
      ['_id' => $this->_id],
      $updates
    );

    return $this;
  }
}

<?php
namespace app\models;

use MongoDB\BSON\UTCDateTime;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\mongodb\ActiveRecord;
use yii\validators\InlineValidator;

use app\libs\DateUtil;
use app\libs\Entity;

use app\models\Media;

class BaseModel extends ActiveRecord
{
  const STATUS_ACTIVE = 10;
	const STATUS_INACTIVE = -1;

	/**
	 * inline custom validator type
	 * add error to model if invalid
	 */
	public function dateTime($attribute)
	{
		if (!$this->$attribute instanceof UTCDateTime)
			$this->addError($attribute, "$attribute is not valid date-time");
	}
	
	/** 
	 * Get an active instance using _id query
	 * active means status = active
	 * @return BaseModel
	 */
	public static function findActiveById($id)
	{
		return self::find()->where(['_id' => $id, 'status' => self::STATUS_ACTIVE])->one();
	}

	/** 
	 * Get an instance using _id query
	 * @return BaseModel
	 */
	public static function findById($id)
	{
		return self::find()->where(['_id' => $id])->one();
	}

	/**
	 * Get model media URL for specific type
	 * @param string $type media Type (e.g. cover, thumbnail, logo)
	 * @param array $params media publishing parameters
	 * @return string
	 */
	public function getMediaUrl($type, $params = [])
	{
		if ($type == 'qr') {
			$url = Yii::$app->encoder->encode([
				Media::ENCODE_ENTITY => Entity::getType($this),
				Media::ENCODE_ID => (string)$this->_id,
				'type' => 'qr',
			] + $params);

			return Url::to("@web/media/$url.png", true);
		}

		$media = Media::find()->where([
				'entity' => Entity::getType($this),
				'refId' => $this->_id,
				'type' => $type,
			])
			->orderBy('_id desc')->one();
		
		return empty($media) ? null : $media->getPublishUrl($params);
	}

	/**
	 * Process attributes' values and generate $set, $unset for mongodb update statement
	 * @param string[] $attrs attributes to update
	 */
  protected function processData($attrs)
  {
    $processedAttrs = [];
		$unsetAttrs = [];

		foreach($attrs as $key => $value)
		{
			if(is_null($value))
			{
				$unsetAttrs[$key] = '';
				continue;
			}
			else {
				// check for dateTime validation
				$validators = $this->getActiveValidators($key);
				foreach($validators as $validator)
				{
					if ($validator instanceof InlineValidator && $validator->method == 'dateTime')
					{
						$value = DateUtil::toUTCDateTime($value);
						break;
					}
				}
			}
			
			if (in_array($key, $this->safeAttributes()))
				$processedAttrs[$key] = $value;
		}
    $processedAttrs['lastUpdateTime'] = new UTCDateTime();
    
    $updates = [
			'$set' => $processedAttrs,
		];
		if (!empty($unsetAttrs))
      $updates['$unset'] = $unsetAttrs;
      
    return $updates;
  }

  /**
	 * Update data using given attributes array
	 * @param string[] $attrs
	 * @param array $params optional parameters
	 * @return bool
	 */
	public function updateData($attrs, $params = [])
	{
		$updates = $this->processData($attrs);

		if (isset($updates['$set']))
			$this->attributes = $updates['$set'];

		if (!$this->validate())
			throw new InvalidParamException('data validation failed: ' . join(', ', array_keys($this->getErrors())));

		return $this->collection->update(
			['_id' => $this->_id],
			$updates
		);
	}
}
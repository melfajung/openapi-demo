<?php

namespace app\controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;

use Yii;
use yii\web\Controller;

use app\libs\Auth;

class OneIdController extends Controller
{
  public function actionIndex()
  {
    $client_id = 139;
    $client_secret = 'ePmB67BjhE8qKrLW2eFCSQXi175EKHqF2nmFx092';

    $username = 'oneid-username';
    $password = 'oneid-password';

    $client = new Client();

    $token = null;
    $tokenFile = Yii::getAlias('@runtime/oneid_token.json');
    // read stored token
    if (file_exists($tokenFile)) {
      $token = json_decode(file_get_contents($tokenFile), true);
    }

    if (empty($token)) {
      // no file or invalid JSON, request for access token
      $fp = fopen($tokenFile, 'w');
      $stream = Psr7\stream_for($fp);
      $client->request('POST', 'https://testoneid.inet.co.th/api/oauth/getpwd', [
        'json' => [
          'grant_type' => 'password',
          'client_id' => $client_id,
          'client_secret' => $client_secret,
          'username' => $username,
          'password' => $password,
        ],
        'save_to' => $stream,
      ]);
      $token = json_decode(file_get_contents($tokenFile), true);
    }
    var_dump($token);

    $res = $client->request('GET', 'https://testoneid.inet.co.th/api/v2/service/shared-token', [
      'headers' => [
        'Authorization' => 'Bearer ' . $token['access_token'],
      ]
    ]);
    $sharedToken = json_decode((string) $res->getBody(), true);
    var_dump($sharedToken);
  }

  public function actionSharetokenInfo()
  {
    $claimTypes = [
      'aud' => 'Audience - who or what token is intended for',
      'exp' => 'Expires At',
      'iat' => 'Issued At',
      'jti' => 'JWT ID',
      'nbf' => 'Not valid before',
      'sub' => 'Subject - whom the token refers to',
    ];
    // call index action to get new token first, shared token is short-lived
    $token = Auth::getJwtToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNmNDhmZmIxYjJjZTRjNmJhYjg3OTQ0ODNkZGM1NDk2ZTYwMWVkN2ExNWM5NjQ4Njc0NTMyN2RhYTA4NDFkZTgwMjc2MGUxYjE4YzdmYjgyIiwia2lkIjoiIn0.eyJhdWQiOiIxMzkiLCJqdGkiOiIzZjQ4ZmZiMWIyY2U0YzZiYWI4Nzk0NDgzZGRjNTQ5NmU2MDFlZDdhMTVjOTY0ODY3NDUzMjdkYWEwODQxZGU4MDI3NjBlMWIxOGM3ZmI4MiIsImlhdCI6MTU4Nzk5NzM0MywibmJmIjoxNTg3OTk3MzQzLCJleHAiOjE1ODc5OTc2NDMsInN1YiI6IjYzNTAzNjI0NDgiLCJ1c2VybmFtZSI6Im1lbGZhanVuZyJ9.oKyRy-jiKV-OluM-HFUtmFkdcMJfiW0R62p7whWpItGRtR-suFfMMwSD2cM1BseupK2Ms3u3Sd2Ml1AgCPE0V-Nr3geH2aJWnGZ7DpLIPRtaFvz5m_GDnWqXs-wT_DXYkzVktwe8VKiqXQSrjzE3rHOYOmvbdW1Cb2CxA8T0rQ971ir-jm786O3_wmbktC4AWHYz6v9OSI2CirWwqalqnj238zz6YQ7Lq7e-0V1ij3dtopnGEGuVxIP_t5l7aAaOT6jRu9y8dal9EEFU9W-1K7Mzz53yt4uA1cDw3KE68D_SrrWi8KjizE2cnBrEV93zkII5aX8ssAgkGlNX_9yhtg');

    if (empty($token)) {
      echo "Token is expired, or invalid";
    } else {
      foreach ($token->getClaims() as $claimId => $claim) {
        $claimInfo = [
          'key' => $claimId,
          'value' => $claim->getValue(),
        ];
        if (isset($claimTypes[$claimId]))
          $claimInfo['meaning'] = $claimTypes[$claimId];

        $claims[] = $claimInfo;
      }
      header('Content-Type: application/json');
      echo json_encode($claims);
      exit;
    }
  }
}

<?php
namespace app\controllers;

use \Yii;
use yii\web\Controller;

use app\models\User;
use app\libs\Auth;

class AuthController extends Controller {
	public function actionInit() {
		$auth = new Auth();
		$auth->init();
	}

	public function actionReassign() {
		$appAuth = new Auth();

		$auth = Yii::$app->authManager;
		$auth->removeAllAssignments();

		$query = User::find();
		foreach($query->all() as $model) {
			$role = $auth->getRole($model->role);
			if(!empty($role))
				$auth->assign($role, $model->username);
		}
		
	}

	public function actionIndex() {
	}
}

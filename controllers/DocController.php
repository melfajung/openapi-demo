<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use cebe\openapi\Reader;

class DocController extends Controller {
  public function actions() {
    return [
        'index' => [
            'class' => 'genxoft\swagger\ViewAction',
            'apiJsonUrl' => \yii\helpers\Url::to(['/doc/file-json'], true),
        ],
        'api-json' => [
            'class' => 'genxoft\swagger\JsonAction',
            'dirs' => [
                Yii::getAlias('@app/models'),
                Yii::getAlias('@app/modules/v1'),
            ],
        ],
    ];
  }

  public function actionFileJson($file = 'main')
  {
    $openapi = Reader::readFromYamlFile(Yii::getAlias("@app/openapi/$file.yml"));

    $json = \cebe\openapi\Writer::writeToJson($openapi);
    return $json;
  }
}

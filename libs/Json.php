<?php
namespace app\libs;

class Json
{
  public static function prepare($data)
  {
    if ($data instanceof \MongoDB\BSON\ObjectId)
    {
      return (string)$data;
    }
    elseif($data instanceof \MongoDB\BSON\UTCDateTime)
    {
      $dt = $data->toDateTime();
      return $dt->format('U') * 1000 + intval($dt->format('u') / 1000);
    }
    elseif (is_iterable($data))
    {
      $result = [];
      foreach($data as $key => $val)
      {
        if (!isset($data[$key])) continue;
        if (is_numeric($key))
          $result[] = self::prepare($val);
        else
          $result[$key] = self::prepare($val);
      }
    }
    else {
      $result = $data;
    }

    return $result;
  }
}

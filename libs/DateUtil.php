<?php
namespace app\libs;

use DateTime;

use MongoDB\BSON\UTCDateTime;

class DateUtil {
  /**
   * convert input data to UTCDateTime instance
   * @param mixed $data
   * @return UTCDateTime
   */
  public static function toUTCDateTime($data)
  {
    $result = null;

    if (is_object($data))
    {
      if ($data instanceof DateTime)
      {
        $result = new UTCDateTime($data->getTimestamp() * 1000);
      }
    }
    elseif (is_string($data))
    {
      $result = new UTCDateTime(strtotime($data) * 1000);
    }

    return $result;
  }
}

<?php
namespace app\libs;

use Yii;

class Entity
{
  /**
   * get entity string define a class type
   * @param Object $obj
   * @return string
   */
  public static function getType($obj)
  {
    return lcfirst(str_replace('app\\models\\', '', get_class($obj)));
  }
  
  public static function getInstance($entityType, $refId)
  {
    try
    {
      $model = call_user_func(["app\models\\" . ucfirst($entityType), 'findById'], $refId);
    }
    catch(\yii\base\ErrorException $ex)
    {
      $model = null;
    }

    return $model;
  }
}

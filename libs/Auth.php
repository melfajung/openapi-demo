<?php

namespace app\libs;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256 as Rs256Signer;
use Lcobucci\JWT\Signer\Hmac\Sha256;

use Yii;
use yii\helpers\Url;

class Auth
{
	private $arrPerm = [
		'app.user' => 'ผู้ใช้งาน',
		'app.user.profile' => 'แก้ไขข้อมูลส่วนตัว',
		'app.debugger' => 'ตรวจสอบระบบ',
		'app.stat.summary' => 'สถิติ'
	];

	public static $arrUserRole = [
		'app.role.admin' => 'ผู้ดูแลระบบ',
		'app.role.staff' => 'ผู้ใช้งาน',
	];

	private $arrRolePerm = [
		'app.role.admin' => ['app.debugger', 'app.user', 'app.user.profile', 'app.stat.summary'],
		'app.role.staff' => ['app.user.profile', 'app.stat.summary'],
	];

	public static $validScopes = ['backend', 'user'];

	/**
	 * Parse bearer authentication token and return Token instance
	 * @param string $token Bearer token to parse
	 */
	public static function getJwtToken($token)
	{
		$token = (new Parser())->parse((string) $token); // Parses from a string
		switch ($token->getHeader('alg')) {
			case 'RS256':
				$signer = new Rs256Signer();
				$signKey = new Key(Yii::$app->params['auth']['rs256Signer']);
				break;
			default:
				$signer = new Sha256();
				$signKey = new Key(Yii::$app->params['auth']['secret']);
		}

		$vData = new ValidationData();
		$vData->setIssuer(Url::base('https'));

		if (!$token->verify($signer, $signKey) || !$token->validate($vData)) {
			return null;
		}

		return $token;
	}

	public function init()
	{
		$auth = Yii::$app->authManager;

		$auth->removeAll();

		foreach ($this->arrPerm as $permName => $title) {
			$perm = $auth->createPermission($permName);
			$perm->description = $title;
			$auth->add($perm);
		}

		foreach (self::$arrUserRole as $roleName => $title) {
			$role = $auth->createRole($roleName);
			$role->description = $title;
			$auth->add($role);

			// assign role permission
			foreach ($this->arrRolePerm[$roleName] as $permName) {
				$perm = $auth->getPermission($permName);
				$auth->addChild($role, $perm);
			}
		}
	}

	/**
	 * Return new JWT token for the current user
	 * @param app\models\User $user user instance
	 * @param integer $duration token age (in seconds)
	 * @return Token
	 */
	public static function newToken($user, $scopes, $duration = 86400)
	{
		$time = time();
		$signer = new Sha256();
		$token = (new Builder())->issuedBy(Url::base('https')) // Configures the issuer (iss claim)
			->expiresAt($time + $duration) // Configures the expiration time of the token (exp claim)
			->withClaim('uid', (string) $user->_id) // Configures a new claim, called "uid"
			->withClaim('roles', $user->roles)
			->withClaim('scopes', $user->scopes)
			->getToken($signer, new Key(Yii::$app->params['auth']['secret'])); // Retrieves the generated token

		return $token;
	}
}
